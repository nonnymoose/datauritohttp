# datauritohttp
This site allows data URIs to be embedded in URL shorteners.
Simply append "#" and the data URI to https://nonnymoose.gitlab.io/datauritohttp

**NOTE: #s in _your_ url should be encoded as %23.**
